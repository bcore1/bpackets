# BPackets - Packet serialization module for Gothic 2 Online

```js
// SHARED-SIDE
enum BonusType {
    DAMAGE,
    HEALTH
}

class BonusItemMessage extends BPacketMessage {
    </ type = BPacketInt32 />
    type = -1

    </ type = BPacketInt32 />
    value = -1
}

class TradeItemMessage extends BPacketMessage {
    </ type = BPacketInt32 />
    id = -1

    </ type = BPacketInt32, optional = true />
    amount = null

    </ type = BPacketArray(BonusItemMessage), optional = true />
    bonuses = null
}

class FriendListMessage extends BPacketMessage {
    </ type = BPacketArray(BPacketObject({id = BPacketInt32, nickname = BPacketString})) />
    friends = null
}

print(TradeItemMessage.format())
// < Packet: 1 >
// + id: int32 = -1
// + amount: int32 = null
// + bonuses: array = null

// SERVER-SIDE
if (SERVER_SIDE) {
    addEventHandler("onPlayerJoin", function (pid) {
        local items = [
            TradeItemMessage(5, 32),
            TradeItemMessage(23, null, [
                BonusItemMessage(BonusType.DAMAGE, 10),
                BonusItemMessage(BonusType.HEALTH, 50)
            ]),
            FriendListMessage([
                {id = 0, nickname = "Bimbol"}
                {id = 1, nickname = "Patrix"}
            ])
        ]

        foreach (message in items) {
            message.serialize().send(pid, RELIABLE)
        }
    })
}

// CLIENT-SIDE
if (CLIENT_SIDE) {
    TradeItemMessage.bind(function (message) {
        print("RECEIVED ITEM:")
        print("id: " + message.id)
        if (message.amount != null) {
            print("amount: " + message.amount)
        }

        if (message.bonuses != null) {
            print("Bonuses:")
            foreach (bonus in message.bonuses) {
                print("+ " + bonus.type + " = " + bonus.value)
            }
        }
    })

    // RECEIVED ITEM:
    // id: 5
    // amount: 32
    // RECEIVED ITEM:
    // id: 23
    // Bonuses:
    // + 0 = 10
    // + 1 = 50

    FriendListMessage.bind(function (message) {
        print("FRIENDS COUNT: " + message.friends.len())

        foreach(friend in message.friends) {
            print("Friend(id = " + friend.id + ", nickname = " + friend.nickname + ")")
        }
    })

    // FRIENDS COUNT: 2
    // Friend(id = 0, nickname = Bimbol)
    // Friend(id = 1, nickname = Patrix)

}
```
